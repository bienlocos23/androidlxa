import json
import io
import sys

props = {
    "cuts" : [("name", True),
              ("price", False),
              ("category",True),
             ],
    "beverage" : [("name", True),
                  ("price", False),
                  ]
}

def to_json(data):
    return json.dumps(data,
                        indent=4,
                        ensure_ascii=False,
                        sort_keys=True)

def new_json(props):
    data = {}
    for prop, isString in props:
        new = input(prop + ': ')
        if not isString:
            new = int(new)
        data[prop] = new
    return data

def add_to_file(wich):
    db = open(wich+'.json', 'r')
    all = json.load(db)

    go = input("\nYou wish to add another object?(y/n) ").upper() == "Y";
    while go:
        with open(wich+'.json', 'w') as db:
            all.append(new_json(props[wich]))
            db.write(to_json(all))
        if input("\nYou wish to add another object?(y/n) ").upper() == "N":
            go = False

if __name__ == '__main__':
    found = False
    while not found:
        wich = input("Wich file do you want to write?")
        try:
            add_to_file(wich)
            found = True
        except FileNotFoundError:
            print("File not found, try again")
