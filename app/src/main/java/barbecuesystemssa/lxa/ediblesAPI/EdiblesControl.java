package barbecuesystemssa.lxa.ediblesAPI;

import java.io.Serializable;
import java.util.List;

public class EdiblesControl implements Serializable {

    private final static String TAG = "EdiblesControl";
    public final static String EDIBLES_CONTROL_KEY = "ediblesControl";

    private int diners;
    private List<BarbecueCut> cart;

    public void setDiners(int din) {
        diners = din;
    }
    public int getDiners() {
        return diners;
    }

    public void setCart(List<BarbecueCut> cuts) { cart = cuts;}
    public List<BarbecueCut> getCart() {return cart;}

}
