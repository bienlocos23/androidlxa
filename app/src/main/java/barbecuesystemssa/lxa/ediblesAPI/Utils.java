package barbecuesystemssa.lxa.ediblesAPI;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import barbecuesystemssa.lxa.ediblesAPI.BarbecueCut;

public class Utils {

    private static final String TAG = "Utils";
    private final static String CUTS_FILE = "cuts.json";

    public static String fromInputStreamToString(InputStream stream) throws IOException {
        BufferedInputStream  bufferedInputStream = new BufferedInputStream(stream);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int result;
        while ((result = bufferedInputStream.read()) != -1){
            byteArrayOutputStream.write((byte) result);
        }
        return byteArrayOutputStream.toString("UTF-8");
    }

    /**
     * JSON shit
     * @return devuelve la lista de los cortes predefinidos
     */
    public static List<BarbecueCut> getCuts(Context ctx) {
        String json = "";
        try {
            json = Utils.fromInputStreamToString(ctx.getAssets().open(CUTS_FILE));
        } catch (IOException ex) {
            Log.e(TAG,"File not found",ex);
        }
        return Arrays.asList(new Gson()
                .fromJson(json, BarbecueCut[].class));
    }

}
