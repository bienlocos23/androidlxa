package barbecuesystemssa.lxa.ediblesAPI;

import java.io.Serializable;

public class BarbecueCut implements Serializable {
    public String name;
    public String category;
    public float price;
    public float how_much;
    public boolean selected = false;

    public boolean isSelected() {return selected;}
    public void setSelected(boolean sel) {selected = sel;}
}
