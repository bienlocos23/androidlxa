package barbecuesystemssa.lxa.recipesAPI;

import java.io.Serializable;

public class Recipe implements Serializable {
    public String title;
    public String description;
    public String url;
}
