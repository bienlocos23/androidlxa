package barbecuesystemssa.lxa.ui.Recipes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import barbecuesystemssa.lxa.Fetchers.CallBack;
import barbecuesystemssa.lxa.Fetchers.ContentFetcher;
import barbecuesystemssa.lxa.R;
import barbecuesystemssa.lxa.recipesAPI.Recipe;

public class RecipesActivity extends AppCompatActivity implements CallBack {
    public static String TAG = "RecipesActivity:";
    List<Recipe> recipes;

    private RecyclerView recipesList;
    private RecipesAdapter recipesAdapter;
    private RecyclerView.LayoutManager recipesLManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipes);

        ContentFetcher.fetchRecipes(this);
        // start creating Recicler View
        recipesList = findViewById(R.id.recipesList);
        // use Linear Layout Manager
        recipesLManager = new LinearLayoutManager(this);
        recipesList.setLayoutManager(recipesLManager);
        //set null adapter
        recipesAdapter = new RecipesAdapter(new ArrayList<Recipe>());
        recipesList.setAdapter(recipesAdapter);
    }

    @Override
    public void onSyncStarted() {
        findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
    }

    @Override
    public void onSyncFinished(boolean error) {
        findViewById(R.id.loadingPanel).setVisibility(View.GONE);
        recipes = ContentFetcher.recipes;

        if(error || recipes.size() == 0)
            Toast.makeText(this, R.string.sync_error, Toast.LENGTH_LONG).show();

        for(Recipe recipe : recipes)
            recipesAdapter.addItem(recipe);

        Log.i(TAG, "Sincronización exitosa");

    }
}


