package barbecuesystemssa.lxa.ui.Recipes;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import barbecuesystemssa.lxa.R;
import barbecuesystemssa.lxa.recipesAPI.Recipe;

public class RecipesAdapter extends RecyclerView.Adapter<RecipesAdapter.ViewHolder> {

    List<Recipe> dataSource;

    public RecipesAdapter(List<Recipe> recipes){
        dataSource = recipes;
    }

    public void addItem(Recipe item) {
        dataSource.add(item);
        notifyItemInserted(dataSource.indexOf(item));
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recipe_cell,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Recipe item = dataSource.get(position);
        Log.i("ADAPTER", item.title);
        holder.recipeTitle.setText(item.title);
        holder.recipeDescription.setText(item.description);
    }

    @Override
    public int getItemCount() {
        return dataSource.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView recipeTitle;
        private TextView recipeDescription;

        ViewHolder(View itemView) {
            super(itemView);
            recipeTitle = itemView.findViewById(R.id.recipe_title);
            recipeDescription = itemView.findViewById(R.id.recipe_description);
        }
    }

}
