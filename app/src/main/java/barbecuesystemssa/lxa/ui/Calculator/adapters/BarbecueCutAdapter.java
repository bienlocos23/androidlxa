package barbecuesystemssa.lxa.ui.Calculator.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import barbecuesystemssa.lxa.R;
import barbecuesystemssa.lxa.ediblesAPI.BarbecueCut;

public class BarbecueCutAdapter extends RecyclerView.Adapter<BarbecueCutAdapter.ViewHolder> {

    static List<BarbecueCut> dataSource;

    public BarbecueCutAdapter(List<BarbecueCut> cuts){
        dataSource = cuts;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cut_cell,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final BarbecueCut item = dataSource.get(position);
        holder.titleCut.setText(item.name);
        holder.priceCut.setText("$".concat(String.valueOf(item.price)));

        //set checkbox attributes
        holder.checkCut.setOnCheckedChangeListener(null);
        holder.checkCut.setSelected(item.selected);
        holder.checkCut.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {item.setSelected(true);}
                else {item.setSelected(false);}
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataSource.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView titleCut;
        private TextView priceCut;
        private CheckBox checkCut;

        public ViewHolder(View itemView) {
            super(itemView);
            priceCut = itemView.findViewById(R.id.price_cut);
            titleCut = itemView.findViewById(R.id.title_cut);
            checkCut = itemView.findViewById(R.id.checkbox_cut);
        }
    }

    public static List<BarbecueCut> getDataSource() {
        return dataSource;
    }
}
