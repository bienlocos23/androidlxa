package barbecuesystemssa.lxa.ui.Calculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;


import java.util.List;

import barbecuesystemssa.lxa.R;
import barbecuesystemssa.lxa.ediblesAPI.BarbecueCut;
import barbecuesystemssa.lxa.ediblesAPI.EdiblesControl;
import barbecuesystemssa.lxa.ui.Calculator.adapters.BarbecueCutAdapter;
import barbecuesystemssa.lxa.ediblesAPI.Utils;

public class CutsActivity extends AppCompatActivity {
    private EdiblesControl ediblesControl;

    private RecyclerView cutsList;
    private RecyclerView.Adapter cutsAdapter;
    private RecyclerView.LayoutManager cutsLManager;
    private BarbecueCutAdapter barbecueCutAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuts);

        //Get cuts information
        ediblesControl = (EdiblesControl) getIntent()
                .getExtras()
                .getSerializable("ediblesControl");
        List<BarbecueCut> cuts = Utils.getCuts(this);

        // Start creating Recycle View
        cutsList = (RecyclerView) findViewById(R.id.cutsList);
        //use linear layout manager
        cutsLManager = new LinearLayoutManager(this);
        cutsList.setLayoutManager(cutsLManager);
        //specify adapter
        barbecueCutAdapter = new BarbecueCutAdapter(cuts);
        cutsList.setAdapter(barbecueCutAdapter);
    }

    public void finalizeCuts(View view) {
        ediblesControl.setCart(barbecueCutAdapter.getDataSource());

        Intent intent = new Intent(this, FinalizeActivity.class);
        intent.putExtra(EdiblesControl.EDIBLES_CONTROL_KEY, ediblesControl);
        startActivity(intent);
    }

}
