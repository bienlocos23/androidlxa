package barbecuesystemssa.lxa.ui.Calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.List;

import barbecuesystemssa.lxa.R;
import barbecuesystemssa.lxa.ediblesAPI.BarbecueCut;
import barbecuesystemssa.lxa.ediblesAPI.EdiblesControl;

public class FinalizeActivity extends AppCompatActivity {
    private EdiblesControl ediblesControl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        System.out.println("FinalizeState");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finalize);

        ediblesControl = (EdiblesControl) getIntent()
                .getExtras()
                .getSerializable(EdiblesControl.EDIBLES_CONTROL_KEY);

        try {
            List<BarbecueCut> cuts = ediblesControl.getCart();
            for (int i = 0; i < cuts.size(); i++) {
                BarbecueCut cut = cuts.get(i);
                if(cut.selected)
                    System.out.println("nombre: " + cut.name);
            }
        } catch (NullPointerException e) {
            System.out.println("No seleccionaste ningún corte");
        }
    }
}
