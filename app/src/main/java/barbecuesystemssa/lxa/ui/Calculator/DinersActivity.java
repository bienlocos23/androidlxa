package barbecuesystemssa.lxa.ui.Calculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import barbecuesystemssa.lxa.R;
import barbecuesystemssa.lxa.ediblesAPI.EdiblesControl;

public class DinersActivity extends AppCompatActivity {
    private final static String TAG = "DinersActivity";
    private EdiblesControl ediblesControl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diners);

        ediblesControl = (EdiblesControl) getIntent()
                .getExtras()
                .getSerializable(EdiblesControl.EDIBLES_CONTROL_KEY);
    }

    public void selectCuts(View view) {
        //set dinners in EdiblesControl
        String diners = ((EditText) findViewById(R.id.diners))
                .getText()
                .toString();

        try {
            ediblesControl.setDiners(Integer.parseInt(diners));
        } catch (NumberFormatException e) {
            Toast.makeText(this, R.string.choose_something_papudo, Toast.LENGTH_LONG)
                    .show();
            return;
        }

        //init cuts selection activity
        Intent intent = new Intent(this, CutsActivity.class);
        intent.putExtra(EdiblesControl.EDIBLES_CONTROL_KEY, ediblesControl);
        startActivity(intent);
    }
}
