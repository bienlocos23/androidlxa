package barbecuesystemssa.lxa.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;
import android.widget.Toast;

import barbecuesystemssa.lxa.R;
import barbecuesystemssa.lxa.ediblesAPI.EdiblesControl;
import barbecuesystemssa.lxa.ui.Calculator.DinersActivity;
import barbecuesystemssa.lxa.ui.Recipes.RecipesActivity;

public class MainActivity extends AppCompatActivity {
    private EdiblesControl ediblesControl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ediblesControl = new EdiblesControl();
    }

    public void startSelection(View view) {
        Intent intent = new Intent(this, DinersActivity.class);
        intent.putExtra(EdiblesControl.EDIBLES_CONTROL_KEY, ediblesControl);
        startActivity(intent);
    }

    public void startRecipes(View view) {
        Intent intent = new Intent(this, RecipesActivity.class);
        startActivity(intent);
    }

    public void dialogue (View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.sorry);
        builder.setMessage(R.string.backdialoge);
        builder.setPositiveButton(R.string.acept, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(MainActivity.this,R.string.sorry,Toast.LENGTH_SHORT).show();
            }
        });
        Dialog dialog = builder.create();
        dialog.show();
    }
}
