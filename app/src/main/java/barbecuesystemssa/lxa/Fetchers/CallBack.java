package barbecuesystemssa.lxa.Fetchers;

public interface CallBack {
    public void onSyncStarted();
    public void onSyncFinished(boolean error);
}
