package barbecuesystemssa.lxa.Fetchers;

import android.os.AsyncTask;
import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import barbecuesystemssa.lxa.recipesAPI.Recipe;

public class ContentFetcher {
    private static String TAG = "ContentFetcher:";

    private static String recipes_url = "http://locosxelasado.com/category/recetas";
    private static Document recipes_doc;

    public static List<Recipe> recipes;

    public static void fetchRecipes(final CallBack context) {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                context.onSyncStarted();
                try {
                    recipes_doc = Jsoup.connect(recipes_url).get();
                } catch (IOException ex) {
                    context.onSyncFinished(true);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                recipes = new ArrayList<>();
                Elements recipes_elems = recipes_doc.getElementsByClass("post-content");
                for (Element elem : recipes_elems) {
                    Recipe recipe = new Recipe();

                    Element title = elem.getElementsByClass("fusion-post-title").first();
                    Element description = elem.getElementsByClass("fusion-post-content-container").first();

                    recipe.title = title.text();
                    recipe.description = description.text();
                    recipe.url = title.select("a").first().attr("href");

                    recipes.add(recipe);
                }
                context.onSyncFinished(false);
            }
        }.execute(null, null, null);

    }
}
