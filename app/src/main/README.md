# ANDROID LXA

## Un software para organizar asados

### Introducción
Una aplicación de android para facilitar el cálculo a la hora de organizar un asado.

### Colaborar
Para colaborar con la creación de la app es necesario tener instalado Andoid Studio (yo estoy usando la versión 3.1). Lo que se encuentra ahora mismo en el repo no es el proyecto completo, para poder correrlo deberán crear su propio proyecto y reemplazar la carpeta:

```
(path/al/proyecto)/app/src/main
```
por la carpeta principal del repo. 
Conviene clonarla usando directamente:

```
git clone https://github.com/rusito-23/ANDROIDLXA (path/al/proyecto)/app/src/main
```


### Objetivos
Aprender Java y desarrollo en Android Studio. El algoritmo de cálculo es lo de menos.
Quizás podría conectar con algúna API para calcular los precios de los cortes en el mercado y calcular el presuspuesto total de la compra.

### Trabajo a futuro
Agregar a la app distintas recetas con distintos cortes, y tips para hacer el asado.
